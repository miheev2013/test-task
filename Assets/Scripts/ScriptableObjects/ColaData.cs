﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ColaData", menuName = "ColaData", order = 53)]
public class ColaData : FoodDataAbstract
{
    [SerializeField]
    private Sprite colaFull;
}
