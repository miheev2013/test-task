﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using U = UnityEngine;
public enum BehaviourMode
{
    None,
    BeginToMoveForOrder,
    MovementForOrder,
    ArrivedAtDestination,
    WaitingForOrder,
    GoBackAfterServed
}

public class Customer : MonoBehaviour
{
    public BehaviourMode BehaviourMode { get; private set; } = BehaviourMode.None;

    private float currentOrderTimeInSec;

    public float WaitOrderTimeNormalized => 1f / gameData.WaitForOrderTime * currentOrderTimeInSec;

    private Vector3 goBackAfterOrderPos;

    private GameData gameData;

    [HideInInspector]
    public Order Order;

    private DestinationPoint currentDestinationPoint;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    private CustomerSpawner customerSpawner;

    private DestinationPointsGroupHandler destinationPointsGroup;

    private Vector3 newMovementPos;
    private Vector3 spawnPos;

    public Action OnServed;

    public void Initialize(CustomerSpawner spawner, DestinationPointsGroupHandler destinationPoints, GameData gameData)
    {
        this.gameData = gameData;
        spawnPos.y = gameData.BaseCharacterActivityPosY;
        goBackAfterOrderPos.y = gameData.BaseCharacterActivityPosY;
        customerSpawner = spawner;
        destinationPointsGroup = destinationPoints;
        Order = new Order();
        Order.Customer = this;

        Order.OnPreorderRemoved += (o) =>
        {
            currentOrderTimeInSec += gameData.BonusTimeAfterPreorder;

            if (currentOrderTimeInSec > gameData.WaitForOrderTime)
                currentOrderTimeInSec = gameData.WaitForOrderTime;
        };

        Order.OnLastPreorderRemoved += () =>
        {
            BehaviourMode = BehaviourMode.GoBackAfterServed;
            SetUpBeforeGoingBack();
        };
    }

    public void Customize(Sprite[] characterSprites, ref int currentSpriteIndex, ref int currentOrderIndex)
    {
        currentOrderTimeInSec = gameData.WaitForOrderTime;
        Sprite sprite = characterSprites[currentSpriteIndex];
        spriteRenderer.sprite = sprite;
        currentSpriteIndex = (currentSpriteIndex + 1) % characterSprites.Length;
        float spawnOffsetX = Camera.main.orthographicSize * 2 + sprite.bounds.extents.x;
        spawnPos.x = U.Random.Range(0, 2) == 0 ? spawnOffsetX : -spawnOffsetX;
        transform.position = spawnPos;
        Order.Preorders = gameData.Orders[currentOrderIndex++].GetPreorderListClone();
        currentDestinationPoint = destinationPointsGroup.GetRandomPoint();
    }

    public void GoForOrder()
    {
        BehaviourMode = BehaviourMode.BeginToMoveForOrder;
    }

    private void Update()
    {
        switch (BehaviourMode)
        {
            case BehaviourMode.None:
                break;
            case BehaviourMode.BeginToMoveForOrder:
                {
                    spriteRenderer.sortingOrder = -1;
                    BehaviourMode = BehaviourMode.MovementForOrder;
                }
                break;
            case BehaviourMode.MovementForOrder:
                {
                    if (transform.position.x != currentDestinationPoint.transform.position.x)
                    {
                        newMovementPos = transform.position;
                        newMovementPos.x = Mathf.MoveTowards(transform.position.x, currentDestinationPoint.transform.position.x, Time.deltaTime * gameData.MoveSpeedCharacter);
                        transform.position = newMovementPos;
                        return;
                    }

                    currentDestinationPoint.OnCustomerCame?.Invoke(Order);
                    spriteRenderer.sortingOrder = 0;
                    currentOrderTimeInSec = gameData.WaitForOrderTime;
                    destinationPointsGroup.UpdateListWithCustomersWaitingForOrder(currentDestinationPoint, this);
                    BehaviourMode = BehaviourMode.WaitingForOrder;
                }
                break;
            case BehaviourMode.WaitingForOrder:
                {
                    if (currentOrderTimeInSec != 0)
                    {
                        currentOrderTimeInSec = Mathf.MoveTowards(currentOrderTimeInSec, 0, Time.deltaTime);
                        return;
                    }

                    BehaviourMode = BehaviourMode.GoBackAfterServed;
                    SetUpBeforeGoingBack();
                }
                break;
            case BehaviourMode.GoBackAfterServed:
                {

                    if (transform.position != goBackAfterOrderPos)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, goBackAfterOrderPos, Time.deltaTime * gameData.MoveSpeedCharacter);
                        return;
                    }

                    customerSpawner.PushIntoPool(this);

                    BehaviourMode = BehaviourMode.None;
                }
                break;
        }
    }

    private void SetUpBeforeGoingBack()
    {
        OnServed?.Invoke();
        destinationPointsGroup.MakePointAsFree(currentDestinationPoint);
        spriteRenderer.sortingOrder = 0;
        float newPosX = Camera.main.orthographicSize * 2 + spriteRenderer.sprite.bounds.extents.x;
        goBackAfterOrderPos.x = U.Random.Range(0, 2) == 0 ? newPosX : -newPosX;
    }

}
