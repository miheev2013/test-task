﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColaMachine : OrderSender
{

    [SerializeField]
    private ColaData data;

    [SerializeField]
    private ColaGlass[] colaGlasses;

    [HideInInspector]
    public int FullGlassesCount;
    
    public void Launch()
    {
        for (int i = 0; i < colaGlasses.Length; i++)
        {
            colaGlasses[i].DoPouring();
        }
    }

    public void OnClick()
    {
        if (FullGlassesCount == 0)
            return;

        if (!RemovePriorityPreorderIfPossible(data))
            return;

        for (int i = 0; i < colaGlasses.Length; i++)
        {
            if (colaGlasses[i].TryGiveCustomer())
            {
                break;
            }
        }
    }

}
