﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trash : MonoBehaviour
{
    [SerializeField]
    private Sprite trashOpen;
    [SerializeField]
    private Sprite trashClose;
    [SerializeField]
    private SpriteRenderer trashState;

    private bool isOpen;

    private float timeIntervalOpenOrigin = 1;
    private float timeIntervalOpenCurrent;

    private void Start()
    {
        timeIntervalOpenCurrent = timeIntervalOpenOrigin;
    }

    private void Update()
    {
        if(isOpen)
        {
            trashState.sprite = trashOpen;
            timeIntervalOpenCurrent = Mathf.Clamp(timeIntervalOpenCurrent -= Time.deltaTime, 0, timeIntervalOpenCurrent);

            if (timeIntervalOpenCurrent == 0)
            {
                isOpen = false;
                timeIntervalOpenCurrent = timeIntervalOpenOrigin;
                trashState.sprite = trashClose;
            }
        }
    }
    [ContextMenu("Open")]
    public void OpenForAWhile()
    {
        isOpen = true;
    }
}
