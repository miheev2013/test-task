﻿using System.Collections.Generic;
using UnityEngine;

public class DestinationPointsGroupHandler : MonoBehaviour
{
    [SerializeField]
    private DestinationPoint[] waitOrderPoints;

    private List<DestinationPoint> freePoints = new List<DestinationPoint>();

    public List<DestinationPoint> PointsWithCustomers { get; } = new List<DestinationPoint>();

    private DestinationPoint randomPointToGoTo;

    public bool FreePointsExist => freePoints.Count > 0;


    public void Initialize()
    {
        freePoints.AddRange(waitOrderPoints);
    }

    public DestinationPoint GetRandomPoint()
    {
        if (freePoints.Count > 1)
        {
            randomPointToGoTo = freePoints[Random.Range(0, freePoints.Count)];
        }
        else if (freePoints.Count == 1)
        {
            randomPointToGoTo = freePoints.Find(fp => fp != null);
        }

        freePoints.Remove(randomPointToGoTo);

        return randomPointToGoTo;
    }

    public void UpdateListWithCustomersWaitingForOrder(DestinationPoint takenPointByCustomer, Customer customer)
    {
        takenPointByCustomer.CustomerWaitingForOrder = customer;
        PointsWithCustomers.Add(takenPointByCustomer);
    }

    public void MakePointAsFree(DestinationPoint destinationPoint)
    {
        destinationPoint.OnCustomerLeft?.Invoke();
        freePoints.Add(destinationPoint);
        PointsWithCustomers.Remove(destinationPoint);
        destinationPoint.CustomerWaitingForOrder = null;
    }
}
