﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaunchGameWindow : MonoBehaviour
{
    [SerializeField]
    private Button launchGameButton;

    [SerializeField]
    private Text text;

    private void Start()
    {
        launchGameButton.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void Show(string txtFormat)
    {
        text.text = string.Format(txtFormat);
        Time.timeScale = 0;
        gameObject.SetActive(true);
    }
}
