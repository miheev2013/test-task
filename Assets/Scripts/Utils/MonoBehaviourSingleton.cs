﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
{
    public static T Instance { get; protected set; }
    public virtual void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Debug.LogErrorFormat($"Объект типа {typeof(T).Name} существует в кол-ве более одного экземпляра");
            Destroy(gameObject);
        }
        else
        {
            Instance = (T)this;
        }
    }
}
