﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FinishLevelResult { Win, Lose }

public class FinishLevelWindow : MonoBehaviour
{
    [SerializeField]
    private Button relaunchGameButton;
    [SerializeField]
    private Button closeAppButton;

    [SerializeField]
    private Text preordersNeedToCompleteTxt;
    [SerializeField]
    private Text completedPreordersTxt;

    private GameData gameData;

    public void Initialize(GameData data)
    {
        gameData = data;
        preordersNeedToCompleteTxt.text = string.Format($"Цель: {gameData.PreordersNeedToComplete}");
    }

    public void Show(FinishLevelResult result, int completedPreorders)
    {
        Time.timeScale = 0;
        gameObject.SetActive(true);
        completedPreordersTxt.text = string.Format($"Отдано: {completedPreorders}");

        if (result == FinishLevelResult.Lose)
        {
            relaunchGameButton.gameObject.SetActive(true);
        }
        else if (result == FinishLevelResult.Win)
        {
            closeAppButton.gameObject.SetActive(true);
        }
    }
}
