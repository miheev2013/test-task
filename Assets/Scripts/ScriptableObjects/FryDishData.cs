﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "FryDishData", menuName = "FryDishData", order = 52)]
public class FryDishData : FoodDataAbstract
{
    public Sprite Pan;
    public Sprite RawCookStateSprite;
    public Sprite NormalCookStateSprite;
    public Sprite BurnCookStateSprite;
    public Sprite PlateSprite;
    public Sprite MainElementPlate;
    public readonly float BurningTime = 7;
}