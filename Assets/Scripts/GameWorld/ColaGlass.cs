﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ColaGlass : MonoBehaviour
{

    [SerializeField]
    private ColaMachine machine;

    [SerializeField]
    private CookTimeIndicator timeIndicator;

    [SerializeField]
    private ColaData data;

    [SerializeField]
    private SpriteRenderer glassEmpty;
    [SerializeField]
    private SpriteRenderer glassEmpty2;
    [SerializeField]
    private SpriteRenderer glassFull;

    private float cookingTimeCurrent;

    private bool isFull = false;

    public void DoPouring()
    {
        timeIndicator.Initialize(data.CookingTime);
        timeIndicator.ExecuteCountDown();

        timeIndicator.OnCookingBegan += () =>
        {
            MakeViewAsEmpty();
        };

        timeIndicator.OnCookingCompleted += () =>
        {
            isFull = true;
            machine.FullGlassesCount++;
            timeIndicator.StopExecution();
            glassEmpty.enabled = false;
            glassEmpty2.enabled = false;
            glassFull.enabled = true;
        };
    }

    private void MakeViewAsEmpty()
    {
        glassEmpty.enabled = true;
        glassEmpty2.enabled = true;
        glassFull.enabled = false;
    }

    public bool TryGiveCustomer()
    {
        if (!isFull)
            return false;

        isFull = false;
        machine.FullGlassesCount--;
        MakeViewAsEmpty();
        timeIndicator.ExecuteCountDown();
        return true;
    }
}
