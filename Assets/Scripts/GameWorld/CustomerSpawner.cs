﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomerSpawner : PoolObjectAbstract<Customer>
{

    [SerializeField]
    private Sprite[] characterSprites;

    private DestinationPointsGroupHandler destinationPoints;

    private GameData gameData;

    private Customer spawnedCustomer;

    private Vector3 spawnPos;

    private int currentOrderIndex;
    private int spawnCount;
    private int orderCount;

    public System.Action<Customer> OnNewCustomerInstantiated;

    private List<Customer> customersInTheScene = new List<Customer>();

    public void Initialize(GameData gameData, DestinationPointsGroupHandler destinationPointsGroupHandler)
    {
        this.gameData = gameData;
        destinationPoints = destinationPointsGroupHandler;
        spawnPos.y = gameData.BaseCharacterActivityPosY;
        orderCount = gameData.Orders.Count();
    }

    public void DoSpawn()
    {
        StartCoroutine(DoSpawning());
    }

    private IEnumerator DoSpawning()
    {
        int currentSpriteIndex = 0;

        while (true)
        {
            yield return new WaitForSeconds(gameData.SpawnTimeInterval);
            
            while (!destinationPoints.FreePointsExist)
                yield return null;

            if (++spawnCount > orderCount)
                yield break;

            spawnedCustomer = GetFromPoolOrNewOne();

            if (!customersInTheScene.Contains(spawnedCustomer))
            {
                customersInTheScene.Add(spawnedCustomer);
                spawnedCustomer.Initialize(this, destinationPoints, gameData);
                spawnedCustomer.Customize(characterSprites, ref currentSpriteIndex, ref currentOrderIndex);
                OnNewCustomerInstantiated?.Invoke(spawnedCustomer);
            }
            else
            {
                spawnedCustomer.Customize(characterSprites, ref currentSpriteIndex, ref currentOrderIndex);
            }

            spawnedCustomer.GoForOrder();
        }
    }
}




