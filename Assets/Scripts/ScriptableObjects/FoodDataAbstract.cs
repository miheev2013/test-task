﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class FoodDataAbstract : ScriptableObject
{
    public BaseOrderType BaseOrderType;

    public float CookingTime = 5;

    public Sprite MainElementSprite;
    public Sprite MainElementSprite2;
}
