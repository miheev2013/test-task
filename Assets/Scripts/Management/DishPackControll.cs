﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct DishPackControl
{
    public FryDishData FryDishData;

    public Pan[] Pans;

    public Plate[] Plates;
}
