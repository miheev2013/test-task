﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderGenerator
{
    private static OrderGenerator instance;
    private OrderGenerator() { }

    public static OrderGenerator Instance
    {
        get
        {
            if (instance == null)
                instance = new OrderGenerator();

            return instance;
        }
    }

    public Order[] GetRandomOrders(int orderCount)
    {
        Order[] orders = new Order[orderCount];

        var randPreorders = new List<Preorder>();

        int randPreorderCount;

        for (int i = 0; i < orders.Length; i++)
        {
            orders[i] = new Order();
            orders[i].Preorders = new List<Preorder>();
            randPreorderCount = Random.Range(1, 4);

            for (int j = 0; j < randPreorderCount; j++)
            {
                var randPreorder = new Preorder();
                int enumRandIndex = Random.Range(1, 4);
                randPreorder.BaseType = (BaseOrderType)enumRandIndex;

                randPreorders.Add(randPreorder);
            }

            orders[i].Preorders.AddRange(randPreorders);
            randPreorders.Clear();
        }

        return orders;
    }
}
