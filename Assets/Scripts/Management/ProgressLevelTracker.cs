﻿using UnityEngine;
using System;
using System.Linq;

public class ProgressLevelTracker
{
    private int preordersNeedToCompleteCount;
    private int completedPreordersCount;
    private int customersNeedToServeCount;
    private int servedCustomersCount;

    public Action<FinishLevelResult, int> OnLevelFinished;

    private int lastNotServedCustomersCount;

    public ProgressLevelTracker(GameData gameData)
    {
        preordersNeedToCompleteCount = gameData.PreordersNeedToComplete;
        customersNeedToServeCount = gameData.Orders.Count();
    }

    public void SubscribeCustomer(Customer customer)
    {
        customer.Order.OnPreorderRemoved += (p) =>
        {
            completedPreordersCount++;
        };

        customer.OnServed += () =>
        {
            if (++servedCustomersCount == customersNeedToServeCount)
            {
                if (completedPreordersCount < preordersNeedToCompleteCount)
                    OnLevelFinished?.Invoke(FinishLevelResult.Lose, completedPreordersCount);
                else
                    OnLevelFinished?.Invoke(FinishLevelResult.Win, completedPreordersCount);
            }
        };
    }
}
