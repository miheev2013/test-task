﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class OrderSender : MonoBehaviour
{

    [SerializeField]
    private DestinationPointsGroupHandler destinationPoints;

    protected bool RemovePriorityPreorderIfPossible(FoodDataAbstract data)
    {
        var c = GetCustomerWithLeastWaitTimeLeft(data);

        if (c == null)
            return false;

        c.Order.RemovePreorderByType(data.BaseOrderType);

        return true;
    }

    private Customer GetCustomerWithLeastWaitTimeLeft(FoodDataAbstract data)
    {
        return destinationPoints.PointsWithCustomers
            .Select(p => p.CustomerWaitingForOrder.Order)
            .Where(o => o.Preorders.Count(po => po.BaseType == data.BaseOrderType) > 0)
            .Select(o => o.Customer)
            .OrderBy(c => c.WaitOrderTimeNormalized)
            .FirstOrDefault();
    }

}
