﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookInputHandler : MonoBehaviour
{
    [SerializeField]
    private DishPackControl burgerPackControl;
    [SerializeField]
    private DishPackControl hotDogPackControl;

    [Space(10)]

    [SerializeField]
    private Trash trash;

    [SerializeField]
    private ColaMachine colaMachine;

    public void Initialize()
    {
        for (int i = 0; i < burgerPackControl.Pans.Length; i++)
        {
            burgerPackControl.Pans[i].Initialize(burgerPackControl.FryDishData);
            burgerPackControl.Pans[i].OnClickWhenStageComplete += PutBurgerMeatOnPlate;
            burgerPackControl.Pans[i].OnDoubleClickWhenBurnedOut += PutItemFromPanIntoTrash;
        }

        for (int i = 0; i < hotDogPackControl.Pans.Length; i++)
        {
            hotDogPackControl.Pans[i].Initialize(hotDogPackControl.FryDishData);
            hotDogPackControl.Pans[i].OnClickWhenStageComplete += PutSausageOnPlate;
            hotDogPackControl.Pans[i].OnDoubleClickWhenBurnedOut += PutItemFromPanIntoTrash;
        }
    }

    public void FryMeat()
    {
        for (int i = 0; i < burgerPackControl.Pans.Length; i++)
        {
            if (burgerPackControl.Pans[i].TryFryIngredient())
            {
                break;
            }
        }
    }

    public void FrySausage()
    {
        for (int i = 0; i < hotDogPackControl.Pans.Length; i++)
        {
            if (hotDogPackControl.Pans[i].TryFryIngredient())
            {
                break;
            }
        }
    }

    public void PutBurgerBreadOnPlate()
    {
        for (int i = 0; i < burgerPackControl.Plates.Length; i++)
        {
            if (burgerPackControl.Plates[i].TryChangeStageToNext(1))
            {
                break;
            }
        }
    }

    public void PutHotDogBreadOnPlate()
    {
        for (int i = 0; i < hotDogPackControl.Plates.Length; i++)
        {
            if (hotDogPackControl.Plates[i].TryChangeStageToNext(1))
            {
                break;
            }
        }
    }

    private void PutBurgerMeatOnPlate(Pan panClicked)
    {
        for (int i = 0; i < burgerPackControl.Plates.Length; i++)
        {
            if (burgerPackControl.Plates[i].TryChangeStageToNext(2))
            {
                panClicked.Reset();
                break;
            }
        }
    }

    private void PutSausageOnPlate(Pan panClicked)
    {
        for (int i = 0; i < hotDogPackControl.Plates.Length; i++)
        {
            if (hotDogPackControl.Plates[i].TryChangeStageToNext(2))
            {
                panClicked.Reset();
                break;
            }
        }
    }

    private void PutItemFromPanIntoTrash(Pan panClicked)
    {
        panClicked.Reset();
        trash.OpenForAWhile();
    }
}


