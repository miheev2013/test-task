﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreorderUI : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> preordersGameObjectList;

    public void DisableAllExceptThisType(BaseOrderType type)
    {
        for (int i = 0; i < preordersGameObjectList.Count; i++)
        {
            if(type.ToString() == preordersGameObjectList[i].tag)
            {
                preordersGameObjectList[i].SetActive(true);
                continue;
            }

            preordersGameObjectList[i].SetActive(false);
        }
    }

    public void Hide()
    {
        for (int i = 0; i < preordersGameObjectList.Count; i++)
        {
            preordersGameObjectList[i].SetActive(false);
        }
    }
}
