﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public enum CookingState { None, Cooking, CookingComplete, BurnedOut }

public class CookTimeIndicator : MonoBehaviour
{
    private CookingState state = CookingState.Cooking;

    [SerializeField]
    private Image backGround;
    [SerializeField]
    private Image progressBar;

    [SerializeField]
    private Sprite greenBack;
    [SerializeField]
    private Sprite burnBack;
    [SerializeField]
    private Sprite greenProgressBar;
    [SerializeField]
    private Sprite burnProgressBar;

    public Action OnCookingBegan;
    public Action OnCookingCompleted;
    public Action OnBurnedOut;

    private float cookingTimeIntervalOrigin;
    private float burningTimeIntervalOrigin;
    private float timeIntervalCurrent;
    private float timeIntervalNormalized;

    private bool withBurnMode;

    private void Update()
    {
        switch (state)
        {
            case CookingState.None:
                gameObject.SetActive(false);
                break;

            case CookingState.Cooking:

                timeIntervalCurrent += Time.deltaTime;
                progressBar.fillAmount = 1f / cookingTimeIntervalOrigin * timeIntervalCurrent;

                if (progressBar.fillAmount == 1)
                {
                    if (withBurnMode)
                    {
                        state = CookingState.BurnedOut;
                        progressBar.fillAmount = 0;
                        backGround.sprite = burnBack;
                        progressBar.sprite = burnProgressBar;
                        timeIntervalCurrent = 0;
                    }
                    else
                    {
                        StopExecution();
                    }

                    OnCookingCompleted?.Invoke();
                }

                break;
            case CookingState.BurnedOut:

                timeIntervalCurrent += Time.deltaTime;
                progressBar.fillAmount = 1f / burningTimeIntervalOrigin * timeIntervalCurrent;

                if (progressBar.fillAmount == 1)
                {
                    StopExecution();
                    OnBurnedOut?.Invoke();
                }

                break;
        }
    }

    public void Initialize(float cookingTimeInterval, float burningTimeInterval = 0)
    {
        cookingTimeIntervalOrigin = cookingTimeInterval;

        if (burningTimeInterval > 0)
        {
            withBurnMode = true;
            burningTimeIntervalOrigin = burningTimeInterval;
        }
    }

    public void ExecuteCountDown()
    {
        progressBar.fillAmount = 0;
        backGround.sprite = greenBack;
        progressBar.sprite = greenProgressBar;
        timeIntervalCurrent = 0;
        state = CookingState.Cooking;
        gameObject.SetActive(true);
        OnCookingBegan?.Invoke();
    }

    public void StopExecution()
    {
        state = CookingState.None;
    }

}
