﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    private CookInputHandler cookInputHandler;

    [SerializeField]
    private LaunchGameWindow launchGameWindow;

    [SerializeField]
    private FinishLevelWindow finishLevelWindow;

    public void Initialize(GameData gameData, ProgressLevelTracker progressLevelTracker)
    {
        launchGameWindow.Show($"Нужно раздать блюд: {gameData.PreordersNeedToComplete}");
        finishLevelWindow.Initialize(gameData);
        cookInputHandler.Initialize();
        progressLevelTracker.OnLevelFinished += finishLevelWindow.Show;
    }

}
