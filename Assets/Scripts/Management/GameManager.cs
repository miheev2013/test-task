﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameData gameData;

    [SerializeField]
    private UIManager uIManager;

    [SerializeField]
    private CustomerSpawner customerSpawner;

    [SerializeField]
    private DestinationPointsGroupHandler destinationPointsGroupHandler;

    [SerializeField]
    private ColaMachine colaMachine;

    private ProgressLevelTracker progressLevelTracker;

    private void Start()
    {
        gameData.Orders = OrderGenerator.Instance.GetRandomOrders(15);
        destinationPointsGroupHandler.Initialize();
        customerSpawner.Initialize(gameData, destinationPointsGroupHandler);
        progressLevelTracker = new ProgressLevelTracker(gameData);

        customerSpawner.OnNewCustomerInstantiated += progressLevelTracker.SubscribeCustomer;
        uIManager.Initialize(gameData, progressLevelTracker);
    }

    public void OnLaunchGame()
    {
        Time.timeScale = 1;
        customerSpawner.DoSpawn();
        colaMachine.Launch();
    }

    public void OnRelaunchGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnAppQuit()
    {
        Application.Quit();
    }
}
