﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class Plate : OrderSender
{
    [SerializeField]
    private FryDishData data;

    [SerializeField]
    private SpriteRenderer plateSpriteRenderer;
    [SerializeField]
    private SpriteRenderer mainSpriteRenderer;
    [SerializeField]
    private SpriteRenderer mainSpriteRenderer2;
    [SerializeField]
    private SpriteRenderer mainSpriteRenderer3;

    private int cookingStageNum;

    public UnityEvent OnResetStage;
    public UnityEvent OnFirstStageOccurred;
    public UnityEvent OnSecondStageOccurred;

    [ContextMenu("Update view")]
    private void UpdateView()
    {
        plateSpriteRenderer.sprite = data.PlateSprite;
        mainSpriteRenderer.sprite = data.MainElementSprite;
        mainSpriteRenderer2.sprite = data.MainElementSprite2;

        mainSpriteRenderer3.sprite = data.MainElementPlate;
    }

    public bool TryChangeStageToNext(int numNextStage)
    {
        int difference = numNextStage - cookingStageNum;

        if (difference != 1)
            return false;

        cookingStageNum = numNextStage;

        if (cookingStageNum == 1)
        {
            OnFirstStageOccurred?.Invoke();
        }
        else if (cookingStageNum == 2)
        {
            OnSecondStageOccurred?.Invoke();
        }

        return true;
    }

    public void OnClick()
    {
        if (cookingStageNum < 2)
            return;

        if (RemovePriorityPreorderIfPossible(data))
        {
            Reset();
        }
    }

    public void Reset()
    {
        cookingStageNum = 0;
        OnResetStage?.Invoke();
    }

}
