﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PoolObjectAbstract<T> : MonoBehaviour
{
    [SerializeField]
    private GameObject poolInstance;

    private Queue<T> queue = new Queue<T>();

    public void PushIntoPool(T item)
    {
        queue.Enqueue(item);
    }

    public T GetFromPoolOrNewOne()
    {
        T newItem;

        if (queue.Count == 0)
        {
            newItem = Instantiate(poolInstance).GetComponent<T>();
        }
        else
        {
            newItem = queue.Dequeue();
        }

        return newItem;
    }
}
