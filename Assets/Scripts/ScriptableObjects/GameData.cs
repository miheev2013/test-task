﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[CreateAssetMenu(fileName = "GameData", menuName = "GameData", order = 51)]
public class GameData : ScriptableObject
{
    [Header("Нахождение персонажей относительно оси Y")]
    public float BaseCharacterActivityPosY = 2;

    [Header("Интервал появления персонажей")]
    public float SpawnTimeInterval = 2;

    [Header("Скорость передвижения персонажей")]
    public float MoveSpeedCharacter = 3;

    [Header("Время ожидания заказа")]
    public float WaitForOrderTime = 3;

    [Header("Бонусное время после сделанного подзаказа")]
    public float BonusTimeAfterPreorder = 6;

    [Header("Все заказы за текущую сессию")]
    public Order[] Orders;

    public int PreordersNeedToComplete
    {
        get
        {
            return Orders
            .SelectMany(o => o.Preorders)
            .Where(p => p.BaseType != BaseOrderType.None).Count() - 2;
        }
    }
}
