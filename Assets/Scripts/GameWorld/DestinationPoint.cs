﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestinationPoint : MonoBehaviour
{
    public UnityEventCustomer OnCustomerCame;

    public UnityEvent OnCustomerLeft;

    [HideInInspector]
    public Customer CustomerWaitingForOrder;

}

