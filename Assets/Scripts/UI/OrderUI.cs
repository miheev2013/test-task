﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderUI : MonoBehaviour
{
    [SerializeField]
    private Slider waitSlider;

    [SerializeField]
    private Transform orderView;

    private Order order;

    private bool isOn;

    [SerializeField]
    private List<PreorderUI> preorderUI;

    public void Show(Order order)
    {
        this.order = order;
        order.OnPreorderRemoved += UpdatePreorders;
        UpdatePreorders(order);
        gameObject.SetActive(true);
        isOn = true;
    }

    public void UpdatePreorders(Order order)
    {
        HideAll();

        if (order.Preorders.Count == 0)
            return;

        for (int i = 0; i < order.Preorders.Count; i++)
        {
            preorderUI[i].DisableAllExceptThisType(order.Preorders[i].BaseType);
        }
    }

    private void HideAll()
    {
        for (int i = 0; i < preorderUI.Count; i++)
        {
            preorderUI[i].Hide();
        }
    }

    private void LateUpdate()
    {
        if (isOn)
            waitSlider.value = order.Customer.WaitOrderTimeNormalized;
    }

    public void HideAndReset()
    {
        order.OnPreorderRemoved -= UpdatePreorders;
        HideAll();
        isOn = false;
        order = null;
        waitSlider.value = 0;
        gameObject.SetActive(false);
    }
}
