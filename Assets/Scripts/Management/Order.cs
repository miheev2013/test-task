﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BaseOrderType { None, Cola, HotDog, Burger }

[System.Serializable]
public class Order
{
    [HideInInspector]
    public Customer Customer;

    public List<Preorder> Preorders;

    public System.Action<Order> OnPreorderRemoved;
    public System.Action OnLastPreorderRemoved;

    public List<Preorder> GetPreorderListClone()
    {
        List<Preorder> preOrdersCopyList = new List<Preorder>();

        for (int i = 0; i < Preorders.Count; i++)
        {
            var preorderCopy = new Preorder();
            preorderCopy.BaseType = Preorders[i].BaseType;
            preOrdersCopyList.Add(preorderCopy);
        }

        return preOrdersCopyList;
    }

    public void RemovePreorderByType(BaseOrderType type)
    {
        for (int i = 0; i < Preorders.Count; i++)
        {
            if (Preorders[i].BaseType == type)
            {
                Preorders.RemoveAt(i);
                OnPreorderRemoved?.Invoke(this);

                if (Preorders.Count == 0)
                    OnLastPreorderRemoved?.Invoke();

                break;
            }
        }
    }
}
