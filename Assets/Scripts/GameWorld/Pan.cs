﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pan : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer panBase;
    [SerializeField]
    private SpriteRenderer cookStateSpriteRenderer;

    private FryDishData panData;

    [SerializeField]
    private CookTimeIndicator timeIndicator;

    private CookingState panState = CookingState.None;

    private float currentTimeDoubleClickValue;

    private Coroutine timeCountDownCoroutine;

    public System.Action<Pan> OnClickWhenStageComplete;
    public System.Action<Pan> OnDoubleClickWhenBurnedOut;

    public void Initialize(FryDishData fryDishData)
    {
        panData = fryDishData;

        timeIndicator.Initialize(panData.CookingTime, panData.BurningTime);

        timeIndicator.OnCookingBegan = () =>
        {
            cookStateSpriteRenderer.enabled = true;
            cookStateSpriteRenderer.sprite = panData.RawCookStateSprite;
        };

        timeIndicator.OnCookingCompleted = () =>
        {
            panState = CookingState.CookingComplete;
            cookStateSpriteRenderer.sprite = panData.NormalCookStateSprite;
        };

        timeIndicator.OnBurnedOut = () =>
        {
            panState = CookingState.BurnedOut;
            cookStateSpriteRenderer.sprite = panData.BurnCookStateSprite;
        };

        StartCoroutine(StartDoubleClickTimer());
    }

    IEnumerator StartDoubleClickTimer()
    {
        while (true)
        {
            currentTimeDoubleClickValue = Mathf.Clamp(currentTimeDoubleClickValue -= Time.deltaTime, 0, currentTimeDoubleClickValue);
            yield return null;
        }
    }

    public bool TryFryIngredient()
    {
        if (panState == CookingState.None)
        {
            panState = CookingState.Cooking;
            timeIndicator.ExecuteCountDown();
            return true;
        }

        return false;
    }

    public void UpdateView()
    {
        panBase.sprite = panData.Pan;
    }

    public void Click()
    {
        if(panState == CookingState.CookingComplete)
        {
            OnClickWhenStageComplete?.Invoke(this);
        }
        else if(panState == CookingState.BurnedOut)
        {
            if(currentTimeDoubleClickValue > 0)
            {
                OnDoubleClickWhenBurnedOut?.Invoke(this);
            }
            else
            {
                currentTimeDoubleClickValue = .5f;
            }
        }
    }

    public void Reset()
    {
        panState = CookingState.None;
        cookStateSpriteRenderer.enabled = false;
        timeIndicator.StopExecution();
    }
}
